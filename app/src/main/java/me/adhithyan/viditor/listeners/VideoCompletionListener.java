package me.adhithyan.viditor.listeners;

import android.media.MediaPlayer;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Created by adhithyan-3592 on 11/04/16.
 */

public class VideoCompletionListener implements MediaPlayer.OnCompletionListener {

    Button button;
    boolean videoCompleted;

    public VideoCompletionListener(Button button){
        this.button = button;
        this.videoCompleted = false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        button.setText("Play");
        videoCompleted = true;
    }

    public boolean isVideoCompleted(){
        return videoCompleted;
    }

    public void videoStarted(){
        videoCompleted = false;
    }
}
