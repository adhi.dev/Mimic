package me.adhithyan.viditor.listeners;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;

import java.io.File;

import me.adhithyan.viditor.R;
import me.adhithyan.viditor.utilities.ToastUtil;

/**
 * Created by adhithyan-3592 on 11/04/16.
 */

public class VideoListClickListener implements AdapterView.OnItemClickListener{

    Context context;

    public VideoListClickListener(Context context){
        this.context = context;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String fileName = (String) parent.getAdapter().getItem(position);

        if(!fileName.equals(context.getString(R.string.no_videos_instruction))){
            File file = new File(fileName);
            Uri video = Uri.fromFile(file);

            Intent intent = new Intent(Intent.ACTION_VIEW, video);
            intent.setDataAndType(video, "video/mp4");
            context.startActivity(intent);
        }
    }
}
