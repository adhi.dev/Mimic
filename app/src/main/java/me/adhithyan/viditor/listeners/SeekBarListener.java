package me.adhithyan.viditor.listeners;

import android.widget.SeekBar;
import android.widget.TextView;

import me.adhithyan.viditor.threads.SeekBarProgressThread;
import me.adhithyan.viditor.utilities.AudioUtil;

/**
 * Created by adhithyan-3592 on 10/04/16.
 */

public class SeekBarListener {
    SeekBar seekBar;

    public SeekBarListener(SeekBar seek){
        seekBar = seek;

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress , boolean fromUser) {
                //AudioUtil.playAudioFromSecond(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }
}
