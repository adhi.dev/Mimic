package me.adhithyan.viditor.activities;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import me.adhithyan.viditor.R;
import me.adhithyan.viditor.adapter.VideoListAdapter;
import me.adhithyan.viditor.listeners.VideoListClickListener;
import me.adhithyan.viditor.utilities.AudioUtil;
import me.adhithyan.viditor.utilities.CameraUtil;
import me.adhithyan.viditor.utilities.ToastUtil;
import me.adhithyan.viditor.utilities.VideoUtil;
import me.adhithyan.viditor.utilities.ViditorConstants;
import me.adhithyan.viditor.utilities.android.AlertUtil;

public class MainActivity extends AppCompatActivity {


    private static final int PICK_AUDIO = 1;
    private static final int PICK_VIDEO = 2;
    Uri audioUri = null, videoUri = null;
    String audio = "audio", video = "video";
    ListView listView;
    List<String> videosList;
    VideoListAdapter adapter;
    VideoListClickListener clickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.app_name));
        AudioUtil.clear();

        clickListener = new VideoListClickListener(this);

        listView = (ListView)findViewById(R.id.listView);

        setListView();
    }

    public void pickAudio(View v){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //Intent intent = new Intent(Intent.ACTION_PICK);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");

        Intent chooser = Intent.createChooser(intent, "Pick an audio file");
        startActivityForResult(chooser, PICK_AUDIO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            switch(requestCode){

                case PICK_AUDIO:
                    audioUri = data.getData();

                    String type = data.resolveType(this);
                    if(type.contains("audio")){
                        AudioUtil.setAudioFile(this, audioUri);
                        Intent intent = new Intent(MainActivity.this, VideoCaptureActivity.class);
                        startActivity(intent);
                    }else{
                        AlertUtil.showAlert(this, getString(R.string.alert_not_an_audio_file_title), getString(R.string.alert_not_an_audio_file_message));
                    }

                    break;
            }
        }else{
            AlertUtil.showAlert(this, getString(R.string.no_audio_chosen_title), getString(R.string.no_audio_chosen_message));
        }
    }


    @Override
    public void onResume(){
        super.onResume();

        setListView();
    }

    private void setListView(){
        videosList = VideoUtil.getVideosList(this);
        adapter = new VideoListAdapter(this, android.R.layout.simple_list_item_1, videosList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(clickListener);

        if(!videosList.get(0).equals(getString(R.string.no_videos_instruction))){
            //ToastUtil.showLongToast(this, getString(R.string.tap_to_play_video));
        }
    }
}
