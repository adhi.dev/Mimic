package me.adhithyan.viditor.activities;

import android.content.Intent;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;

import me.adhithyan.viditor.R;
import me.adhithyan.viditor.listeners.SeekBarListener;
import me.adhithyan.viditor.threads.SeekBarProgressThread;
import me.adhithyan.viditor.utilities.AudioUtil;
import me.adhithyan.viditor.utilities.CameraUtil;
import me.adhithyan.viditor.utilities.ToastUtil;
import me.adhithyan.viditor.utilities.VideoUtil;
import me.adhithyan.viditor.utilities.ViditorConstants;

public class VideoCaptureActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private MediaRecorder recorder;
    private SurfaceHolder holder;
    SurfaceView camView;
    private static String videoFile = null;
    SeekBar seekBar;
    RelativeLayout layout;
    int audioDuration = AudioUtil.getAudioDurationInMillis() / 1000;
    Thread t;
    SeekBarProgressThread progressThread;
    Button camButton;
    android.hardware.Camera camera = null;
    TextView tapCameraText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_capture);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.app_name));

        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBar.setMax(audioDuration);


        tapCameraText = (TextView)findViewById(R.id.textViewTapCamera);

        SeekBarListener seekBarListener = new SeekBarListener(seekBar);
        AudioUtil.setSeekBar(seekBar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        camView = (SurfaceView)findViewById(R.id.surfaceView);
        holder = camView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(ViditorConstants.recording){
            recorder.stop();
            AudioUtil.stopPlayback();
            ViditorConstants.recording = false;
        }
    }

    private void initRecorder(String videoFile){


        recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        recorder.setOutputFile(videoFile);
        recorder.setMaxDuration(AudioUtil.getAudioDurationInMillis() + 200);
        //recorder.setPreviewDisplay(holder.getSurface());
    }

    private void prepareRecorder(){

        //recorder.setPreviewDisplay(holder.getSurface());

        try{
            recorder.prepare();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void captureVideo(View v){

        videoFile = VideoUtil.getOutputFileString(this, ViditorConstants.VIDEO);
        ViditorConstants.fileName = videoFile;

        if(videoFile != null){
            if(!ViditorConstants.recording){
                tapCameraText.setText("");
                initRecorder(videoFile);
                prepareRecorder();

                ViditorConstants.recording = true;

                releaseResourcesAfterCapturing();

                recorder.start();
                AudioUtil.playAudio();
            }

        }else{
            ToastUtil.showLongToast(this, "Unable to record video at this time. Please try after some time.");
        }

    }

    @Override
    public void onBackPressed(){
        releaseResources();
        finish();
    }

    public void startPreview(){
        if(recorder == null){
            recorder = new MediaRecorder();
        }

        camera = android.hardware.Camera.open(CameraUtil.getFrontCamId());
        try {
            camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        camera.setDisplayOrientation(90);
        camera.startPreview();
        camera.unlock();

        recorder.setCamera(camera);
        recorder.setPreviewDisplay(holder.getSurface());
    }

    private void releaseResourcesAfterCapturing(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                releaseResources();

                Intent intent = new Intent(VideoCaptureActivity.this, VideoViewerActivity.class);
                startActivity(intent);
                finish();
            }
        }, AudioUtil.getAudioDurationInMillis() + 250);
    }

    private void releaseResources(){
        ViditorConstants.recording = false;

        if(recorder != null){
            recorder.release();
        }

        if(camera != null){
            camera.release();

        }

        AudioUtil.destroyThread();
    }

}
