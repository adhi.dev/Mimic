package me.adhithyan.viditor.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;

import me.adhithyan.viditor.R;
import me.adhithyan.viditor.listeners.VideoCompletionListener;
import me.adhithyan.viditor.threads.VideoDeletionThread;
import me.adhithyan.viditor.utilities.ToastUtil;
import me.adhithyan.viditor.utilities.ViditorConstants;

public class VideoViewerActivity extends AppCompatActivity {

    VideoView video;
    VideoCompletionListener videoCompletionListener;
    Button stateButton;
    TextView locationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_viewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.mimic_video_player));


        stateButton = (Button)findViewById(R.id.stateButton);
        locationTextView = (TextView)findViewById(R.id.textViewRevert);

        String fileName = ViditorConstants.fileName.substring(ViditorConstants.fileName.lastIndexOf(File.separator) + 1, ViditorConstants.fileName.length());
        locationTextView.setText(locationTextView.getText() + fileName);

        videoCompletionListener = new VideoCompletionListener(stateButton);

        video = (VideoView)findViewById(R.id.videoView);
        video.setVideoPath(ViditorConstants.fileName);
        video.setMediaController(new MediaController(this));
        video.requestFocus();
        video.setOnCompletionListener(videoCompletionListener);
        video.start();
    }

    public void retakeVideo(View v){

        if(video.isPlaying()){
            video.stopPlayback();
        }

        VideoDeletionThread deletionThread = new VideoDeletionThread(ViditorConstants.fileName);
        Thread delete = new Thread(deletionThread);
        delete.start();

        Intent intent = new Intent(VideoViewerActivity.this, VideoCaptureActivity.class);
        startActivity(intent);

        finish();
    }

    public void controlPlayback(View v){
        if(video.isPlaying()){
            video.pause();
            stateButton.setText("Play");
        }else{
            if(videoCompletionListener.isVideoCompleted()){
                videoCompletionListener.videoStarted();
                video.start();
            }else{
                video.start();
            }

            stateButton.setText("Pause");
        }
    }

    public void share(View v){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("video/mp4");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(ViditorConstants.fileName)));

        startActivity(Intent.createChooser(intent, getString(R.string.intent_share_title)));
    }

}
