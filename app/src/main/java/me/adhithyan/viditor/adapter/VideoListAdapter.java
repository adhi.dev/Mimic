package me.adhithyan.viditor.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.HashMap;
import java.util.List;

/**
 * Created by adhithyan-3592 on 11/04/16.
 */

public class VideoListAdapter extends ArrayAdapter<String> {

    public HashMap<String, Integer> videolocaterMap;

    public VideoListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);

        videolocaterMap = new HashMap<String, Integer>();

        for(int i=0; i<objects.size(); i++){
            videolocaterMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position){
        String item = getItem(position);
        return videolocaterMap.get(item);
    }

    @Override
    public boolean hasStableIds(){
        return true;
    }

}
