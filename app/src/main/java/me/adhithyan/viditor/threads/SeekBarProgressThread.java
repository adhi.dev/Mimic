package me.adhithyan.viditor.threads;

import android.widget.SeekBar;
import android.widget.TextView;

import me.adhithyan.viditor.utilities.AudioUtil;

/**
 * Created by adhithyan-3592 on 10/04/16.
 */

public class SeekBarProgressThread implements Runnable{
    SeekBar seekBar;
    int duration;
    boolean progressing;
    int i;

    public SeekBarProgressThread(SeekBar seekBar, int duration){
        this.seekBar = seekBar;
        this.duration = duration;
        progressing = true;
        i = 0;

    }

    @Override
    public void run() {

        for(; i<=duration; i++){
            while(!progressing){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            seekBar.setProgress(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void pauseProgress(){
        progressing = false;
    }

    public void resumeProgress(){
        progressing = true;
    }

    public boolean isProgressingPaused(){
        return !progressing;
    }

    public int getCurrentProgress(){
        return i;
    }

    public void setProgress(int i){
        this.i = i;
    }

    public void resetProgress(){
        this.i = 0;
        seekBar.setProgress(0);
    }
}
