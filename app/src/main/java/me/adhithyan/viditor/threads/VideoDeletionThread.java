package me.adhithyan.viditor.threads;

import java.io.File;

/**
 * Created by adhithyan-3592 on 11/04/16.
 */

public class VideoDeletionThread implements Runnable {
    String fileName;

    public VideoDeletionThread(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void run() {
        File file = new File(fileName);
        file.delete();
    }

}
