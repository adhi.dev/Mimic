package me.adhithyan.viditor.utilities;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by adhithyan-3592 on 08/04/16.
 */

public class ToastUtil {

    public static void showLongToast(Context context, String content){
        Toast.makeText(context, content, Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(Context context, String content){
        Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
    }

}
