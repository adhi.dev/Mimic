package me.adhithyan.viditor.utilities;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import me.adhithyan.viditor.threads.SeekBarProgressThread;

/**
 * Created by adhithyan-3592 on 10/04/16.
 */

public class AudioUtil {

    private static MediaPlayer audioPlayer = null;
    private static SeekBarProgressThread seekBarProgressThread;
    private static SeekBar seekBar;
    static boolean seeked = false;
    static Thread t = null;

    private static int getAudioDuration(Context context, Uri fileUri){
        MediaMetadataRetriever metaData = new MediaMetadataRetriever();
        metaData.setDataSource(context, fileUri);

        String duration = metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        return Integer.parseInt(duration);
    }

    public static int getAudioDurationInMillis(){
        return audioPlayer.getDuration();
    }

    public static int getDurationInSeconds(){
        return audioPlayer.getDuration() / 1000;
    }

    public static void setAudioFile(Context context, Uri uri){
        String audioPath = getPathFromUri(context, uri);

        audioPlayer = new MediaPlayer();
        audioPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            audioPlayer.setDataSource(context, uri);
            audioPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void playAudio(){

        if(seekBarProgressThread == null){
            seekBarProgressThread = new SeekBarProgressThread(seekBar, getAudioDurationInMillis()/1000);

        }


        if(!audioPlayer.isPlaying()){

            int seekBarProgress = seekBarProgressThread.getCurrentProgress();


            seekBarProgressThread.resetProgress();

            if(t != null && t.isAlive()){
                t.destroy();
            }

            t = new Thread(seekBarProgressThread);
            t.start();

            audioPlayer.start();
        }


    }

    public static void stopPlayback(){
        if(audioPlayer.isPlaying()){
            audioPlayer.pause();
            seekBarProgressThread.pauseProgress();

            destroyThread();
        }
    }


    private static String getPathFromUri(Context context, Uri uri){
        String[] projection = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, projection, null, null, null);

        Cursor cursor = loader.loadInBackground();
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(columnIndex);
    }

    public static void setSeekBar(SeekBar seekBar1){
        seekBar = seekBar1;
    }

    public static void clear(){
        seeked = false;

    }

    public static void destroyThread(){
        if(t != null && t.isAlive()){
            //t.destroy();
        }

        if(seekBarProgressThread != null){
            seekBarProgressThread = null;
        }
    }

    public static boolean isAudioFile(String fileName){
        if(fileName.contains("media")){
            return true;
        }

        String extension = fileName.substring(fileName.lastIndexOf('.'), fileName.length());

        if(extension.equals("mp3") || extension.equals("aac")){
            return true;
        }

        return false;
    }

}
