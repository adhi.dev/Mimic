package me.adhithyan.viditor.utilities.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import me.adhithyan.viditor.utilities.ViditorConstants;

/**
 * Created by adhithyan-3592 on 10/04/16.
 */

public class AlertUtil {


    public static void showAlert(Context context, String title, String message){
        ViditorConstants.OK_ALERT = false;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ViditorConstants.OK_ALERT = true;
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
