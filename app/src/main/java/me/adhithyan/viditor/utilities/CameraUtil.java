package me.adhithyan.viditor.utilities;

import android.hardware.Camera;

/**
 * Created by adhithyan-3592 on 10/04/16.
 */

public class CameraUtil {

    public static int getFrontCamId(){
        int i = -1;

        for(i=0; i< Camera.getNumberOfCameras(); i++){
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);

            if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){

                break;
            }

        }
       return i;
    }
}
