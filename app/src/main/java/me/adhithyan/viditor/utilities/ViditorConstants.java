package me.adhithyan.viditor.utilities;

/**
 * Created by adhithyan-3592 on 08/04/16.
 */

public class ViditorConstants {
    public static final int AUDIO = 1;
    public static final int VIDEO = 2;

    public static final int CAPTURE_VIDEO = 100;

    public static boolean OK_ALERT = false;
    public static boolean recording = false;

    public static String fileName = null;
}
