package me.adhithyan.viditor.utilities;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.adhithyan.viditor.R;

/**
 * Created by adhithyan-3592 on 08/04/16.
 */

public class VideoUtil {
    public static Uri getOutputMediaFileUri(Context context, int type){
        File file = getOutputMediaFile(context, type);

        if(file != null){
            return Uri.fromFile(file);
        }else{
            return null;
        }
    }

    public static File getOutputMediaFile(Context context, int type){
        File mediaStorageDir = getVideoDirectory(context);

        if(mediaStorageDir == null){
            return null;
        }

        return new File(getOutputFileString(context, type));
    }

    public static String getOutputFileString(Context context, int type){
        File mediaStorageDir = getVideoDirectory(context);

        if(mediaStorageDir == null){
            return null;
        }

        Date date = new Date();
        String timestamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(date.getTime());

        return mediaStorageDir.getPath() + File.separator + "Viditor_" + timestamp + ".mp4";
    }

    private static File getVideoDirectory(Context context){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "Viditor");
        //ToastUtil.showLongToast(context, mediaStorageDir.getAbsolutePath());

        if(!mediaStorageDir.exists()){
            ToastUtil.showLongToast(context, "Creating a new directory named Viditor in SD Card");
            if(!mediaStorageDir.mkdirs()){
                ToastUtil.showLongToast(context, "Unable to create directory Viditor in SD Card");
                return null;
            }
        }

        return mediaStorageDir;
    }

    public static List<String> getVideosList(Context context){
        List<String> videoList = new ArrayList<String>();

        File directory = getVideoDirectory(context);

        if(directory.isDirectory()){
            File[] files = directory.listFiles();

            for(File file:files){
                videoList.add(file.getPath());
            }
        }

        if(videoList.size() == 0){
            videoList.add(context.getString(R.string.no_videos_instruction));
        }

        return videoList;
    }
}
