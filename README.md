# Mimic - Android app
A Dubsmash clone developed for Venturesity Roposo Social Hack - Round A.

## Table of contents

* [Installation](#installation)
* [License](#license)

## Installation

Download the apk from [here](https://github.com/v-adhithyan/Mimic/releases/download/v1.0/mimic.apk) and install.

## License

Released under MIT license. See [license file](https://github.com/v-adhithyan/Mimic/blob/master/LICENSE.md) for more info.



